# Commands

## test

```sh
$SNAPSHOT_TEST_COMMAND >./output/actual
delta --navigate --keep-plus-minus-markers --line-numbers --paging=never --true-color=never ./output/accepted ./output/actual
```

## accept

```sh
$SNAPSHOT_TEST_COMMAND >./output/accepted
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
$SNAPSHOT_TEST_COMMAND "$NAME"
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    nix develop --command \
        devbox update
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
