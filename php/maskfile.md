# Commands

## test

```sh
./vendor/bin/phpunit
```

### test watch

```sh
./vendor/bin/phpunit-watcher watch
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
if [ -n "$NAME" ]; then
    bin/main "$NAME"
else
    bin/main
fi
```

## install

```sh
composer install
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    nix develop --command \
        devbox update
direnv exec . \
    nix develop --command \
        composer update
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
