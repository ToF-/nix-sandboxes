<?php

declare(strict_types=1);

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use function App\hello;

final class HelloTest extends TestCase
{
    public function testHelloWorld()
    {
        $this->assertSame("Hello world", hello());
    }

    public function testHelloFoo()
    {
        $this->assertSame("Hello foo", hello("foo"));
    }
}
