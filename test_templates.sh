#!/usr/bin/env bash

set -o errexit

for_each() {
    local FUNCTION="$1"
    shift

    while read -r TEMPLATE; do
        "$FUNCTION" "$TEMPLATE" "$@"
    done
}

list_templates() {
    ls --directory ./*/ |
        xargs --replace basename {} |
        grep --invert-match legacy
}

template_that_break_test() {
    grep --invert-match "typescript_bun" | # broken when using in /tmp
        grep --invert-match "erlang"       # stop the test script
}

test_template() {
    local TEMPLATE="$1"
    local TEMP_DIR="${2:-/tmp}"

    # https://github.com/NixOS/nix/issues/8355#issuecomment-1551712655
    # https://github.com/NixOS/nix/issues/7154#issuecomment-1505656602
    unset TMPDIR

    nix flake new --quiet --template ".#$TEMPLATE" "$TEMP_DIR/$TEMPLATE"
    pushd "$TEMP_DIR/$TEMPLATE"
    direnv allow
    direnv exec . sh -c 'eval $GAMBLE_TEST_COMMAND'
    popd
}

main() {
    local TEMP_DIR
    TEMP_DIR="$(mktemp --directory)"

    list_templates |
        template_that_break_test |
        for_each test_template "$TEMP_DIR"

    rm --recursive --force "$TEMP_DIR"
}

MAIN=${1:-main}
shift &>/dev/null || true

$MAIN $*
