-module(mylib).

-import(string, [concat/2]).

%% API exports
-export([main/1, hello/1]).

%%====================================================================
%% API functions
%%====================================================================

%% escript Entry point
main(Args) ->
    io:format("~s~n", [hello(Args)]),
    erlang:halt(0).

hello("") -> "Hello world";
hello(Name) -> concat("Hello ", Name).

%%====================================================================
%% Internal functions
%%====================================================================
