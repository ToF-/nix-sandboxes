-module(hello_test).
-import(mylib, [hello/1]).
-include_lib("eunit/include/eunit.hrl").
-export([]).

hello_test_() ->
    [
        ?_assert(hello("") =:= "Hello world"),
        ?_assert(hello("foo") =:= "Hello foo")
    ].
