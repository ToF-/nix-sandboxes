# Nix Sandboxes

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Sandboxes / starters for differents languages

## Requirements

* Install [Git](https://git-scm.com)
* Install [Nix](https://nixos.org)
* Install [DirEnv](https://direnv.net)

## Usage

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/pinage404/nix-sandboxes)

### Automatic

#### List sandboxes

```sh
NIX_CONFIG='extra-experimental-features = flakes nix-command' \
nix run 'gitlab:pinage404/nix-sandboxes'
```

#### Get one sandbox

e.g. for Rust

```sh
NIX_CONFIG='extra-experimental-features = flakes nix-command' \
nix run 'gitlab:pinage404/nix-sandboxes' rust ./your_new_project_directory
```

### Manual

1. There is 2 possibles ways
    * **Lighten / Faster**
        * Take just one langage

          e.g. for Rust

          ```sh
          NIX_CONFIG="extra-experimental-features = flakes nix-command" \
          nix flake new --template "gitlab:pinage404/nix-sandboxes#rust" ./your_new_project_directory
          ```

        * Move to the directory

          ```sh
          cd ./your_new_project_directory
          ```

    * **Complete**
        * Clone this repository

          ```sh
          git clone https://gitlab.com/pinage404/nix-sandboxes.git
          cd nix-sandboxes
          ```

        * Move to the directory

          e.g. for Rust

          ```sh
          cd rust
          ```

1. Enable DirEnv

    This will trigger Nix to get and setup everything that is needed

    ```sh
    direnv allow
    ```

1. *Optional steps*
    * If you use VSCode
        * Install [VSCode](https://code.visualstudio.com)
        * Open from the command line in the folder

          ```sh
          code .
          ```

        * Install recommanded extensions

    * To avoid rebuilding things that have already been built elsewhere, use Cachix
        * Install [Cachix](https://www.cachix.org/)
        * Use Nix's cache

          ```sh
          cachix use pinage404-nix-sandboxes
          ```

    * If you use [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md) : a tool that blend TCR + TDD to make sure to **develop** the **right** thing, **babystep by babystep**
        * Install [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md#how-to-install)
            * If you cloned the repository, it's already included
            * If you used a template, in the `.envrc` uncomment `use flake gitlab:pinage404/git-gamble?rev=c088fb269c7d6ac46d66cf71d54005d8a8ed8213&dir=/packaging/nix/flake/`

              ```diff
              -# use flake "gitlab:pinage404/git-gamble?rev=c088fb269c7d6ac46d66cf71d54005d8a8ed8213&dir=/packaging/nix/flake/"
              +use flake "gitlab:pinage404/git-gamble?rev=c088fb269c7d6ac46d66cf71d54005d8a8ed8213&dir=/packaging/nix/flake/"
              ```

        * Tests should pass

          ```sh
          git gamble --pass
          ```

1. Enjoy

## Usefull links

### Exercices / Katas

List of exercices / katas

* [Kata Log](https://kata-log.rocks/index.html)
* [List of excercices](https://github.com/cyber-dojo/exercises-start-points/tree/master/start-points)
  * from [cyber-dojo](https://cyber-dojo.org/creator/home)
* [Samman Technical Coaching's Katas](https://sammancoaching.org/kata_descriptions/index.html)
* [Emily Bache's Katas](https://github.com/emilybache?tab=repositories)
* [Coding Dojo's Katas](https://codingdojo.org/kata/)
* [Xavier Nopre's Katas](https://github.com/xnopre/xnopre-katas)

### Help with languages

List of tools to help writing in some language

* [Programming-Idioms](https://programming-idioms.org/)
* [Syntax Cheatsheet for Javascript/Python](https://www.theyurig.com/blog/javascript-python-syntax)
* [Awesome Lists](https://awesome.digitalbunker.dev/) curated lists of links related to a topic
* [Hello, World! in differents languages on Wikipedia](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program#Examples)

### Others Bootstraps

List of alternatives bootstraps / setup / template with more languages

* [kata-bootstraps of Softwerkskammer Berlin](https://github.com/swkBerlin/kata-bootstraps)
* [CodeWorks' Katapult](https://github.com/CodeWorksFrance/katapult)
* [List of setup](https://github.com/orgs/cyber-dojo-start-points/repositories?type=source)
  * from [cyber-dojo](https://cyber-dojo.org/creator/home)
* [NixOS's templates](https://github.com/NixOS/templates)
* [DevEnv](https://devenv.sh/)
* [DevShell](https://numtide.github.io/devshell/)
* [DevBox](https://www.jetpack.io/devbox)
* [Flox](https://floxdev.com/)
* More on [Awesome Nix](https://github.com/nix-community/awesome-nix#development)

## Do you like this project ?

<!-- markdownlint-disable-next-line MD039 MD045 -->
* If yes, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
* If no, please [open an issue](https://gitlab.com/pinage404/nix-sandboxes/-/issues) to give your feedback

## Contributing

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/nix-sandboxes/-/issues), [merge request](https://gitlab.com/pinage404/nix-sandboxes/-/merge_requests) ...) are welcome

### Adding a language

Follow [`CONTRIBUTING.md`](./CONTRIBUTING.md)

---

Fork of [`FaustXVI/sandboxes`](https://github.com/FaustXVI/sandboxes.git)
