# Forth with test

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=forth/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#forth" ./your_new_project_directory
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Awesome Forth](https://github.com/lassik/awesome-forth#readme)

[Gforth cheat sheet](https://gist.github.com/rickcarlino/9578850)

[GForth](https://gforth.org)

[Forth Foundation Library](https://irdvo.nl/FFL/)
