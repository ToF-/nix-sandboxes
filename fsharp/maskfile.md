# Commands

## test

```sh
dotnet test
```

### test watch

```sh
watchexec --clear --exts fs -- $MASK test
```

## lint

```sh
fd '\.fs$' --exec dotnet fsharplint lint {}
```

## format

```sh
dotnet fantomas .
```

## install

```sh
dotnet tool restore
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    nix develop --command \
        devbox update
direnv exec . \
    nix develop --command \
        dotnet tool update fantomas
direnv exec . \
    nix develop --command \
        dotnet tool update dotnet-fsharplint
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
